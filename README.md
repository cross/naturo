# Naturo
Naturo is a Proof Of Skill (PoS) web application for booking and viewing nature hikes and outdoor camping, as well as reading/posting blog posts related to hiking.

# What to expect:

## Automation
    Automatically add shifts to staff members google calenders
    Automatically email a receipt for bookings

## Administrator Dashboard
    View, Manage and Refund hiking/camping sessions
    View, Manage and Remove blog posts
    Assign shifts to staff

## Staff Dashboard
    View shifts, routes and maps
    Write blog posts

## Landing Page
    Parallax Scrolling Page - Animated SVG of campfire, hammock between two trees and trees surrounding
    FAQ
    Safety measures - Ensure not only safety of children, but also of hikers
    Information about business
    Pictures of previous trips
    Meet the leaders - Staff names, pictures, quotes, experience.
    Have link to advice page

## Purchase Page
    Pricing Calculator - Manage time periods, over weekends, weekdays, per child and adult, family pass, price variation based on local holidays
    Have link to advice page

## Advice Page
    What to expect
    What to bring

## Blog
    Picture support
    Rich editing - Markdown? Custom MD Engine
    Comments, Likes?

## Purchase Handling
    Offload to services like PayPal etc.
