package database

import (
	"naturo/database/models"
	"naturo/database/repositories"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	Bookings *repositories.BookingRepository
	Users    *repositories.UserRepository
)

func Connect() (*gorm.DB, error) {
	db, err := gorm.Open(mysql.Open(os.Getenv("DSN")), &gorm.Config{})

	db.AutoMigrate(&models.Booking{}, &models.Party{}, &models.Trip{}, &models.User{}, &models.Authorization{})

	if err != nil {
		return nil, err
	}

	Bookings = repositories.NewBookingRepository(db)
	Users = repositories.NewUserRepository(db)

	return db, nil
}
