package models

import (
	"time"

	"gorm.io/gorm"
)

type TripType string

const (
	HIKE TripType = "HIKE"
	CAMP TripType = "CAMP"
)

func (tt *TripType) Scan(value interface{}) error {
	*tt = TripType(value.([]byte))
	return nil
}

type Booking struct {
	gorm.Model

	Code string

	Party Party
	Trip  Trip

	HasBeenPaid bool
}

type Party struct {
	Adults     int
	Children   int
	FamilyPass bool

	BookingID int
}

type Trip struct {
	Start *time.Time
	End   *time.Time
	Type  TripType

	BookingID int
}
