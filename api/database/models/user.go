package models

import "gorm.io/gorm"

type User struct {
	gorm.Model

	DisplayName string

	Authorization Authorization
}

type Authorization struct {
	Username string
	Password string
	Email    string
	UserID   int
}
