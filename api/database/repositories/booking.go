package repositories

import (
	"naturo/database/models"

	"gorm.io/gorm"
)

type BookingRepository struct {
	db *gorm.DB
}

func NewBookingRepository(database *gorm.DB) *BookingRepository {
	return &BookingRepository{
		db: database,
	}
}

func (b *BookingRepository) CreateNewBooking(booking models.Booking) (int64, error) {
	result := b.db.Create(&booking)
	return result.RowsAffected, result.Error
}

func (b *BookingRepository) FindBookingByCode(code string) (models.Booking, error) {
	Booking := models.Booking{}
	result := b.db.First(&Booking, "code = ?", code)
	return Booking, result.Error
}

func (b *BookingRepository) FindFullBookingByCode(code string) (models.Booking, error) {
	// TODO Change this function
	Booking := models.Booking{}

	booking := b.db.First(&Booking, "code = ?", code)

	if booking.Error != nil {
		return Booking, booking.Error
	}

	party := b.db.First(&Booking.Party, "booking_id = ?", Booking.ID)

	if party.Error != nil {
		return Booking, party.Error
	}

	trip := b.db.First(&Booking.Trip, "booking_id = ?", Booking.ID)

	if trip.Error != nil {
		return Booking, trip.Error
	}

	return Booking, nil
}

func (b *BookingRepository) DeleteBookingByCode(code string) (int64, error) {
	result := b.db.Where("code = ?", code).Delete(&models.Booking{})
	return result.RowsAffected, result.Error
}
