package repositories

import (
	"naturo/database/models"

	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(database *gorm.DB) *UserRepository {
	return &UserRepository{
		db: database,
	}
}

func (b *UserRepository) CreateNewUser(user models.User) (int64, error) {
	result := b.db.Create(&user)
	return result.RowsAffected, result.Error
}

func (u *UserRepository) GetByUsername(username string) (models.User, error) {
	User := models.User{}
	_, auth, err := u._GetByUsername(username)
	if err != nil {
		return User, err
	}
	result := u.db.First(&User, "id = ?", auth.UserID)
	return User, result.Error
}

func (u *UserRepository) GetByEmail(email string) (models.User, error) {
	User := models.User{}
	_, auth, err := u._GetByEmail(email)
	if err != nil {
		return User, err
	}
	result := u.db.First(&User, "id = ?", auth.UserID)

	Auth := u.db.First(&User.Authorization, "user_id = ?", User.ID)

	if Auth.Error != nil {
		return User, Auth.Error
	}

	return User, result.Error
}

func (u *UserRepository) _GetByEmail(email string) (*gorm.DB, models.Authorization, error) {
	Auth := models.Authorization{}
	result := u.db.First(&Auth, "email = ?", email)
	return result, Auth, result.Error
}

func (u *UserRepository) _GetByUsername(username string) (*gorm.DB, models.Authorization, error) {
	Auth := models.Authorization{}
	result := u.db.First(&Auth, "username = ?", username)
	return result, Auth, result.Error
}
