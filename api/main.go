package main

import (
	"log"
	"naturo/database"
	"naturo/router"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"
)

type Application struct {
	Logger   *log.Logger
	Router   *mux.Router
	Database *gorm.DB
}

func main() {
	app := Initialize()
	app.Start()
}

func Initialize() Application {
	app := Application{}
	app.Router = router.CreateRouter()
	app.Logger = log.New(os.Stdout, "[>] ", log.Lshortfile)

	if db, err := database.Connect(); err != nil {
		if err != nil {
			app.Logger.Fatal(err)
		} else {
			app.Database = db
		}
	}

	return app
}

func (app Application) Start() {
	app.Logger.Print("=========")
	app.Router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, _ := route.GetPathTemplate()
		met, _ := route.GetMethods()
		name := route.GetName()
		app.Logger.Println(tpl, met, name)
		return nil
	})
	app.Logger.Print("=========")
	app.Logger.Print("Listening")

	app.Logger.Fatal(http.ListenAndServe(":80", app.Router))
}

/*
	TODO
		Hide personal details in routes
			-> Email, Password
			-> Booking Code
*/
