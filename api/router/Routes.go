package router

import (
	"naturo/router/booking"
	"naturo/router/generic"
	"naturo/router/user"

	"net/http"
)

type Route struct {
	Name    string
	Method  string
	Pattern string
	Handler http.HandlerFunc
}

var routes = []Route{
	{
		"Index",
		"GET",
		"/",
		generic.Index,
	},
	{
		"Create_Booking",
		"POST",
		"/booking/create",
		booking.Create,
	},
	{
		"View_Booking",
		"GET",
		"/booking/code/{code}",
		booking.View,
	},
	{
		"Delete_Booking",
		"DELETE",
		"/booking/code/{code}",
		booking.Delete,
	},
	{
		"User_Register",
		"POST",
		"/user/register",
		user.Register,
	},
	{
		"User_Login",
		"POST",
		"/user/login",
		user.Login,
	},
	{
		"User_Current",
		"POST",
		"/user/current",
		user.Current,
	},
	{
		"User_ByEmail",
		"GET",
		"/user/email/{email}",
		user.ByEmail,
	},
}
