package booking

import (
	"encoding/json"
	"fmt"
	repos "naturo/database"
	"naturo/database/models"
	"naturo/router/response"
	"net/http"
	"time"

	util "naturo/utils"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
)

// TODO Remove lines that output "<error>.Error()", this is bad for production
// TODO Make routes enforce authoriazation - have links to user (Create route stores creator's user ID etc.)

var (
	validate *validator.Validate = validator.New()
)

func Create(w http.ResponseWriter, r *http.Request) {
	createParams := CreateBody{}

	err := json.NewDecoder(r.Body).Decode(&createParams)

	if err != nil {
		response.Error(w, "The provided data could not be decoded!")
		return
	}

	err = validate.Struct(createParams)

	if err != nil {
		var fields []string

		for _, err := range err.(validator.ValidationErrors) {
			fields = append(fields, err.Field())
		}

		response.Error(w, "Please correct these fields!", fields)
		return
	}

	now := time.Now()

	code := now.Format("2106") + "-NR-" + util.RandStringBytes(7)

	party := models.Party{
		Adults:     createParams.Party.Adult,
		Children:   createParams.Party.Child,
		FamilyPass: *createParams.Party.HasFamilyPass,
	}

	start, _ := time.Parse("2006-01-02 15:04:05", createParams.Trip.Start)
	end, _ := time.Parse("2006-01-02 15:04:05", createParams.Trip.End)

	trip := models.Trip{
		Start: &start,
		End:   &end,
		Type:  models.TripType(createParams.Trip.Type),
	}

	booking := models.Booking{
		Code:        code,
		Party:       party,
		Trip:        trip,
		HasBeenPaid: false,
	}

	repos.Bookings.CreateNewBooking(booking)

	response.Success(w, booking)
}

func View(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	code := params["code"]

	err := validate.Var(code, "required")

	if err != nil {
		response.Error(w, "Invalid Booking Code")
		return
	}

	var booking models.Booking
	booking, err = repos.Bookings.FindFullBookingByCode(code)

	if err != nil {
		response.Error(w, err.Error())
		return
	}

	response.Success(w, booking)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	code := params["code"]

	err := validate.Var(code, "required")

	if err != nil {
		response.Error(w, "Invalid Booking Code")
		return
	}

	rows, err := repos.Bookings.DeleteBookingByCode(code)

	if err != nil {
		response.Error(w, "An error occured")
		return
	}

	response.Success(w, fmt.Sprintf("%v rows affected", rows))
}
