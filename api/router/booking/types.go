package booking

type CreateBody struct {
	Party Party `json:"party"`
	Trip  Trip  `json:"trip"`
}

type Party struct {
	Adult         int   `json:"adultCount" validate:"gte=1,lte=5,required"`
	Child         int   `json:"childCount" validate:"gte=1,lte=5,required"`
	HasFamilyPass *bool `json:"hasFamilyPass" validate:"required"`
}

type Trip struct {
	Start string `json:"start" validate:"required"`
	End   string `json:"end" validate:"required"`
	Type  string `json:"type" validate:"required"`
}
