package response

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	Success bool        `json:"success"`
	Reason  string      `json:"reason,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func Error(w http.ResponseWriter, message string, data ...interface{}) {
	res := Response{
		Success: false,
		Reason:  message,
		Data:    data,
	}
	json.NewEncoder(w).Encode(res)
}

func Success(w http.ResponseWriter, data interface{}) {
	res := Response{
		Success: true,
		Data:    data,
	}
	json.NewEncoder(w).Encode(res)
}
