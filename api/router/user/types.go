package user

type CreateBody struct {
	Username string
	Password string
	Email    string
}
