package user

import (
	"encoding/json"
	repos "naturo/database"
	"naturo/database/models"
	"naturo/router/response"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
)

var (
	validate *validator.Validate = validator.New()
)

func Register(w http.ResponseWriter, r *http.Request) {
	// TODO
	createParams := CreateBody{}

	err := json.NewDecoder(r.Body).Decode(&createParams)

	if err != nil {
		response.Error(w, "The provided data could not be decoded!")
		return
	}

	err = validate.Struct(createParams)

	if err != nil {
		var fields []string

		for _, err := range err.(validator.ValidationErrors) {
			fields = append(fields, err.Field())
		}

		response.Error(w, "Please correct these fields!", fields)
		return
	}

	response.Success(w, "Working")
}

func Login(w http.ResponseWriter, r *http.Request) {
	// TODO
	response.Success(w, "Working")
}

func Current(w http.ResponseWriter, r *http.Request) {
	// TODO
	response.Success(w, "Working")
}

func ByEmail(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	email := params["email"]

	err := validate.Var(email, "required")

	if err != nil {
		response.Error(w, "Invalid Email")
		return
	}

	var user models.User
	user, err = repos.Users.GetByEmail(email)

	if err != nil {
		response.Error(w, err.Error())
		return
	}

	response.Success(w, user)
}
